package com.arqui.aulavirtual.presentation.main.homeworkdetail;

import android.content.Context;

import com.arqui.aulavirtual.data.entities.HomeworksEntity;
import com.arqui.aulavirtual.data.entities.ModuleEntity;
import com.arqui.aulavirtual.data.local.SessionManager;
import com.arqui.aulavirtual.data.remote.ServiceFactory;
import com.arqui.aulavirtual.data.remote.request.ListRequest;
import com.arqui.aulavirtual.presentation.main.homeworks.HomeworkItem;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by katherine on 31/05/17.
 */

public class HomeworkDetailPresenter implements HomeworkDetailContract.Presenter, HomeworkDetailItem {

    private final HomeworkDetailContract.View mView;
    private final SessionManager mSessionManager;
    private Context context;
    private boolean firstLoad = false;
    private int currentPage = 1;

    public HomeworkDetailPresenter(HomeworkDetailContract.View mView, Context context) {
        this.mView = mView;
        this.mSessionManager = new SessionManager(context);
        this.mView.setPresenter(this);

    }



    @Override
    public void start() {
    }



    @Override
    public void getPdf(int id) {

    }


    @Override
    public void clickItem(ModuleEntity jobEntity) {
        mView.clickItemDetail(jobEntity);
    }

    @Override
    public void deleteItem(ModuleEntity jobEntity, int position) {

    }
}
