package com.arqui.aulavirtual.presentation.auth;

import android.content.Context;
import android.support.annotation.NonNull;

import com.arqui.aulavirtual.data.entities.AccessTokenEntity;
import com.arqui.aulavirtual.data.entities.UserEntity;
import com.arqui.aulavirtual.data.local.SessionManager;
import com.arqui.aulavirtual.data.remote.ServiceFactory;
import com.arqui.aulavirtual.data.remote.request.LoginRequest;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by katherine on 10/05/17.
 */

public class LoginPresenter implements LoginContract.Presenter {

    private final LoginContract.View mView;
    private Context context;
    private final SessionManager mSessionManager;

    public LoginPresenter(@NonNull LoginContract.View mView, @NonNull Context context) {
        this.context = checkNotNull(context, "context cannot be null!");
        this.mView = checkNotNull(mView, "newsView cannot be null!");
        this.mView.setPresenter(this);
        mSessionManager = new SessionManager(context);
    }

    @Override
    public void loginUser(final String username, final String password) {
        LoginRequest loginService =
                ServiceFactory.createService(LoginRequest.class);
        Call<ArrayList<UserEntity>> call = loginService.login("f81bff7ea450e52b208d4a9e32a1cc01",
                "json", "core_user_get_users_by_field", "username", username);
        mView.setLoadingIndicator(true);
        call.enqueue(new Callback<ArrayList<UserEntity>>() {
            @Override
            public void onResponse(Call<ArrayList<UserEntity>> call, Response<ArrayList<UserEntity>> response) {
                if(!mView.isActive()){
                    return;
                }
                if (response.isSuccessful()) {

                    switch (response.code()){
                        case 200:
                            openSession("f81bff7ea450e52b208d4a9e32a1cc01", response.body().get(0));
                            break;
                        case 202:
                            mView.setLoadingIndicator(false);
                            mView.errorLogin("Verifica si tu cuenta es de administrador o usuario");
                            break;

                    }
                } else {
                    mView.setLoadingIndicator(false);
                    mView.errorLogin("Login Fallido");
                }
            }

            @Override
            public void onFailure(Call<ArrayList<UserEntity>> call, Throwable t) {
                if(!mView.isActive()){
                    return;
                }
                mView.setLoadingIndicator(false);
                mView.errorLogin("No se puede conectar al servidor");
            }
        });
    }



    @Override
    public void openSession(String token, UserEntity userEntity) {

        mSessionManager.openSession(token);
        mSessionManager.setUser(userEntity);
        mView.setLoadingIndicator(false);
        mView.loginSuccessful(userEntity);
    }

    @Override
    public void start() {

    }
}
