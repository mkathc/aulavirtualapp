package com.arqui.aulavirtual.presentation.main.homeworks;


import com.arqui.aulavirtual.core.BasePresenter;
import com.arqui.aulavirtual.core.BaseView;
import com.arqui.aulavirtual.data.entities.CourseEntity;
import com.arqui.aulavirtual.data.entities.HomeworksEntity;

import java.util.ArrayList;

/**
 * Created by katherine on 31/05/17.
 */

public interface HomeworkContract {
    interface View extends BaseView<Presenter> {


        void getHomeworks(ArrayList<HomeworksEntity> list);


        void clickItemHomework(HomeworksEntity jobEntity);

        void responseSendData(String message);

        boolean isActive();



    }

    interface Presenter extends BasePresenter {

        void loadListHomeworks(int id);



    }
}
