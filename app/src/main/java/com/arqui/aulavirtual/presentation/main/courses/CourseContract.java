package com.arqui.aulavirtual.presentation.main.courses;


import com.arqui.aulavirtual.core.BasePresenter;
import com.arqui.aulavirtual.core.BaseView;
import com.arqui.aulavirtual.data.entities.CourseEntity;
import com.arqui.aulavirtual.data.entities.HomeworksEntity;

import java.util.ArrayList;

/**
 * Created by katherine on 31/05/17.
 */

public interface CourseContract {
    interface View extends BaseView<Presenter> {

        void getCourseList(ArrayList<CourseEntity> list);

        void getHomeworks(ArrayList<HomeworksEntity> list);


        void clickItemCourse(CourseEntity jobEntity);

        void responseSendData(String message);

        boolean isActive();



    }

    interface Presenter extends BasePresenter {

        void loadListHomeworks(int id);

        void loadList();

        void sendDatoForJob(int idJob, int idUser);

    }
}
