package com.arqui.aulavirtual.presentation.main.homeworkdetail;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v13.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.webkit.URLUtil;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.arqui.aulavirtual.core.BaseActivity;
import com.arqui.aulavirtual.core.BaseFragment;
import com.arqui.aulavirtual.core.RecyclerViewScrollListener;
import com.arqui.aulavirtual.core.ScrollChildSwipeRefreshLayout;
import com.arqui.aulavirtual.data.entities.HomeworksEntity;
import com.arqui.aulavirtual.data.entities.ModuleEntity;
import com.arqui.aulavirtual.data.local.SessionManager;
import com.arqui.aulavirtual.presentation.main.homeworks.HomeworkItem;
import com.arqui.aulavirtual.presentation.profile.ProfileActivity;
import com.arqui.aulavirtual.utils.ProgressDialogCustom;
import com.arqui.jobs.R;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by katherine on 31/05/17.
 */

public class HomeworkDetailFragment extends BaseFragment implements HomeworkDetailContract.View {

    public static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 1024;

    @BindView(R.id.rv_list)
    RecyclerView rvList;
    @BindView(R.id.noListIcon)
    ImageView noListIcon;
    @BindView(R.id.noListMain)
    TextView noListMain;
    @BindView(R.id.noList)
    LinearLayout noList;
    @BindView(R.id.refresh_layout)
    ScrollChildSwipeRefreshLayout refreshLayout;
    Unbinder unbinder;
    @BindView(R.id.profile)
    FloatingActionButton profile;
    private String url;
    private String nameOfFile;

    private HomeworkDetailAdapter mAdapter;
    private LinearLayoutManager mLayoutManager;
    private HomeworkDetailContract.Presenter mPresenter;
    private ProgressDialogCustom mProgressDialogCustom;
    private SessionManager mSessionManager;
    private HomeworksEntity homeworksEntity;
    private int courseId;

    public HomeworkDetailFragment() {
        // Requires empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
        getModules(homeworksEntity.getModules());

    }

    public static HomeworkDetailFragment newInstance(Bundle bundle) {
        HomeworkDetailFragment fragment = new HomeworkDetailFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSessionManager = new SessionManager(getContext());
        getActivity().registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));

    }

    BroadcastReceiver onComplete = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            DownloadManager.Query query = new DownloadManager.Query();
            query.setFilterById(intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, 0L));
            DownloadManager manager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
            Cursor cursor = manager.query(query);
            while (cursor.moveToNext()) {
                if (cursor.getCount() > 0) {
                    int status = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS));
                    if (status == DownloadManager.STATUS_SUCCESSFUL) {
                        String file = cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI));
                        if (file != null) {
                            File mFile = new File(Uri.parse(file).getPath());
                            if (mFile.exists()) {
                                Toast.makeText(getContext(), "Archivo descargado", Toast.LENGTH_LONG).show();
                                if (mProgressDialogCustom.isShowing()) {
                                    mProgressDialogCustom.dismiss();
                                }
                                    if (Build.VERSION.SDK_INT < 24) {
                                        intent = new Intent(Intent.ACTION_VIEW);
                                        intent.setDataAndType(Uri.fromFile(mFile), "application/pdf");
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                    } else {
                                        intent = new Intent(Intent.ACTION_VIEW);
                                        Uri photoURI = FileProvider.getUriForFile(getContext(),
                                                getContext().getApplicationContext().getPackageName() + ".com.osiptel.osiptelApp.provider", mFile);
                                        intent.setDataAndType(photoURI, "application/pdf");
                                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                        startActivity(intent);
                                    }

                                    try {
                                        if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                                            startActivity(intent);
                                        } else
                                            Toast.makeText(context, "No se encontraron aplicaciones para abrir el archivo", Toast.LENGTH_SHORT).show();
                                    } catch (Exception e) {
                                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                                    }

                            }
                        }
                        // So something here on success
                    } else if (status == DownloadManager.STATUS_FAILED) {
                        // int message = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_REASON));
                        // So something here on failed.
                        if (mProgressDialogCustom.isShowing()) {
                            mProgressDialogCustom.dismiss();
                        }
                        Toast.makeText(getActivity(), "El archivo no se encuentra disponible", Toast.LENGTH_LONG).show();
                    }
                }
            }
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_list, container, false);
        final ScrollChildSwipeRefreshLayout swipeRefreshLayout =
                (ScrollChildSwipeRefreshLayout) root.findViewById(R.id.refresh_layout);
        swipeRefreshLayout.setColorSchemeColors(
                ContextCompat.getColor(getActivity(), R.color.black),
                ContextCompat.getColor(getActivity(), R.color.dark_gray),
                ContextCompat.getColor(getActivity(), R.color.black)
        );
        // Set the scrolling view in the custom SwipeRefreshLayout.
        swipeRefreshLayout.setScrollUpChild(rvList);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //mPresenter.start();
                //mPresenter.loadListHomeworks(mSessionManager.getUserEntity().getIdUser());
            }
        });
        homeworksEntity = (HomeworksEntity) getArguments().getSerializable("homeworksEntity");
        unbinder = ButterKnife.bind(this, root);
        return root;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_profile, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_profile:
                //Toast.makeText(getActivity(), "Favoritos", Toast.LENGTH_SHORT).show();
                nextActivity(getActivity(), null, ProfileActivity.class, false);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mProgressDialogCustom = new ProgressDialogCustom(getContext(), "Descargando...");
        mLayoutManager = new LinearLayoutManager(getContext());
        rvList.setLayoutManager(mLayoutManager);
        mAdapter = new HomeworkDetailAdapter(new ArrayList<ModuleEntity>(), getContext(), (HomeworkDetailItem) mPresenter);
        rvList.setAdapter(mAdapter);

    }

    public void getModules(ArrayList<ModuleEntity> list) {

        mAdapter.setItems(list);
        if (list != null) {
            noList.setVisibility((list.size() > 0) ? View.GONE : View.VISIBLE);
        }

        rvList.addOnScrollListener(new RecyclerViewScrollListener() {
            @Override
            public void onScrollUp() {

            }

            @Override
            public void onScrollDown() {

            }

            @Override
            public void onLoadMore() {
                // mPresenter.loadListHomeworks(mSessionManager.getUserEntity().getIdUser());
            }
        });
    }

    @Override
    public void clickItemDetail(ModuleEntity jobEntity) {
        if(jobEntity.getModplural().equals("Archivos")){
            url = jobEntity.getContents().get(0).getFileurl()+"&token=f81bff7ea450e52b208d4a9e32a1cc01";
            if (checkSelfPermission()) {
                getDowloadUrl();
            }
        }else{
            String url = "http://35.239.193.129/moodle/";
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
        }
    }

    private void getDowloadUrl(){

        if (url != null) {
            downloadFile();
        } else {
            Toast.makeText(getContext(), "El formato no se encuentra disponible", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void responseSendData(String message) {

    }

    @Override
    public boolean isActive() {
        return isAdded();
    }


    @Override
    public void setPresenter(HomeworkDetailContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void setLoadingIndicator(final boolean active) {
        if (getView() == null) {
            return;
        }
        final SwipeRefreshLayout srl =
                (SwipeRefreshLayout) getView().findViewById(R.id.refresh_layout);

        // Make sure setRefreshing() is called after the layout is done with everything else.
        srl.post(new Runnable() {
            @Override
            public void run() {
                srl.setRefreshing(active);
            }
        });

        if (active) {
            mProgressDialogCustom.show();
        } else {
            if (mProgressDialogCustom.isShowing()) {
                mProgressDialogCustom.dismiss();
            }
        }
    }

    @Override
    public void showMessage(String message) {
        ((BaseActivity) getActivity()).showMessage(message);
    }

    @Override
    public void showErrorMessage(String message) {
        ((BaseActivity) getActivity()).showMessageError(message);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();

    }

    @OnClick(R.id.profile)
    public void onViewClicked() {
        Bundle bundle = new Bundle();
        bundle.putBoolean("myProfile", true);
        nextActivity(getActivity(), bundle, ProfileActivity.class, false);
    }

    public void downloadFile() {
        mProgressDialogCustom.show();
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
        request.setTitle("Descargando archivo");
        request.setDescription("File is being downloaded...");
        request.allowScanningByMediaScanner();
        nameOfFile = URLUtil.guessFileName(url, null, MimeTypeMap.getFileExtensionFromUrl(url));
        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath(), nameOfFile);
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setDestinationUri(Uri.fromFile(file));
        DownloadManager manager = (DownloadManager) getContext().getSystemService(Context.DOWNLOAD_SERVICE);
        manager.enqueue(request);
    }

    private boolean checkSelfPermission() {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getContext());
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permisos Necesarios!");
                    alertBuilder.setMessage("Para descargar el archivo es necesario brindar los permisos correspondientes");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();
                } else {
                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getDowloadUrl();
                } else {
                    Toast.makeText(getContext(), "Debe aceptar los permisos para iniciar la descarga", Toast.LENGTH_SHORT).show();
                    //code for deny
                }
                break;
        }
    }
}
