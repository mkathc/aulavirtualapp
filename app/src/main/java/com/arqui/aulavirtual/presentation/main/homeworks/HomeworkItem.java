package com.arqui.aulavirtual.presentation.main.homeworks;

import com.arqui.aulavirtual.data.entities.CourseEntity;
import com.arqui.aulavirtual.data.entities.HomeworksEntity;

/**
 * Created by kath on 2/06/18.
 */

public interface HomeworkItem {

    void clickItem(HomeworksEntity jobEntity);

    void deleteItem(HomeworksEntity jobEntity, int position);
}
