package com.arqui.aulavirtual.presentation.profile;

import com.arqui.aulavirtual.core.BasePresenter;
import com.arqui.aulavirtual.core.BaseView;
import com.arqui.aulavirtual.data.entities.UserEntity;

/**
 * Created by katherine on 21/06/17.
 */

public interface ProfileContract {
    interface View extends BaseView<Presenter> {
        void ShowSessionInformation(UserEntity userEntity);
        boolean isActive();
    }

    interface Presenter extends BasePresenter {

    }
}
