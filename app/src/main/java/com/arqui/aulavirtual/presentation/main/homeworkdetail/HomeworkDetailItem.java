package com.arqui.aulavirtual.presentation.main.homeworkdetail;

import com.arqui.aulavirtual.data.entities.HomeworksEntity;
import com.arqui.aulavirtual.data.entities.ModuleEntity;

/**
 * Created by kath on 2/06/18.
 */

public interface HomeworkDetailItem {

    void clickItem(ModuleEntity jobEntity);

    void deleteItem(ModuleEntity jobEntity, int position);
}
