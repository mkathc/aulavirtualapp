package com.arqui.aulavirtual.presentation.auth;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.arqui.aulavirtual.presentation.main.homeworks.HomeworksActivity;
import com.arqui.aulavirtual.presentation.main.user.CoursesActivity;
import com.arqui.jobs.R;
import com.arqui.aulavirtual.core.BaseActivity;
import com.arqui.aulavirtual.core.BaseFragment;
import com.arqui.aulavirtual.data.entities.UserEntity;
import com.arqui.aulavirtual.presentation.register.RegisterActivity;
import com.arqui.aulavirtual.utils.ProgressDialogCustom;
import com.facebook.CallbackManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by junior on 27/08/16.
 */
public class LoginFragment extends BaseFragment implements LoginContract.View, Validator.ValidationListener,
        GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = LoginActivity.class.getSimpleName();


    CallbackManager mCallbackManager;
    @NotEmpty
    @BindView(R.id.et_email)
    EditText etEmail;
    @NotEmpty
    @BindView(R.id.et_password)
    TextInputEditText etPassword;
    @BindView(R.id.btn_login)
    Button btnLogin;
    @BindView(R.id.et_register)
    TextView etRegister;
    Unbinder unbinder;

    private LoginContract.Presenter mPresenter;
    private ProgressDialogCustom mProgressDialogCustom;
    private Validator validator;
    private int userType;
    private boolean isLoading = false;

    private static final int RC_SIGN_IN = 007;


    public LoginFragment() {
        // Requires empty public constructor
    }

    public static LoginFragment newInstance(Bundle bundle) {
        LoginFragment fragment = new LoginFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCallbackManager = CallbackManager.Factory.create();
        //LoginManager.getInstance().registerCallback(mCallbackManager, this);

        userType = getArguments().getInt("userType");


    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.start();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_login_complete, container, false);
        unbinder = ButterKnife.bind(this, root);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mProgressDialogCustom = new ProgressDialogCustom(getContext(), "Ingresando...");
        validator = new Validator(this);
        validator.setValidationListener(this);
    }


    @Override
    public void setPresenter(LoginContract.Presenter mPresenter) {
        this.mPresenter = mPresenter;
    }

    @Override
    public void setLoadingIndicator(boolean active) {
        if (getView() == null) {
            return;
        }
        if (active) {
            mProgressDialogCustom.show();
        } else {
            if (mProgressDialogCustom.isShowing()) {
                mProgressDialogCustom.dismiss();
            }
        }
    }

    @Override
    public void showMessage(String msg) {
        ((BaseActivity) getActivity()).showMessage(msg);
    }

    @Override
    public void showErrorMessage(String message) {
        ((BaseActivity) getActivity()).showMessageError(message);
    }

    @Override
    public void loginSuccessful(UserEntity userEntity) {

        /*switch (userType){
            case 1:
                newActivityClearPreview(getActivity(), null, HomeworksDetailActivity.class);
                break;
            case 2:
                break;

        }*/
        newActivityClearPreview(getActivity(), null, CoursesActivity.class);
        showMessage("Login exitoso");
    }

    private void getMainActivity(int userType){
        switch (userType){
            case 1:
                newActivityClearPreview(getActivity(), null, HomeworksActivity.class);
                break;
            case 2:
                //newActivityClearPreview(getActivity(), null, CoursesActivity.class);
                break;

            default:
                break;
        }
    }

    @Override
    public void errorLogin(String msg) {
        showErrorMessage(msg);
    }


    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public void onValidationSucceeded() {
        mPresenter.loginUser(etEmail.getText().toString(), etPassword.getText().toString());
        isLoading = true;


    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getContext());
            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(getContext(), "Por favor ingrese lo campos correctamente", Toast.LENGTH_SHORT).show();
            }
        }
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @OnClick({R.id.btn_login, R.id.et_register})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_login:
                validator.validate();
                break;
            case R.id.et_register:
                Bundle bundle = new Bundle();
                bundle.putInt("userType", userType);
                nextActivity(getActivity(),bundle, RegisterActivity.class, false);
                break;
        }
    }
}