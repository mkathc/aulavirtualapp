package com.arqui.aulavirtual.data.remote.request;

import com.arqui.aulavirtual.data.entities.AccessTokenEntity;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by katherine on 10/05/17.
 */

public interface RegisterRequest {

    @FormUrlEncoded
    @POST("users/register")
    Call<AccessTokenEntity> registerUser(@Field("userEmail") String email,
                                         @Field("userName") String first_name,
                                         @Field("userLastName1") String last_name,
                                         @Field("userPassword") String password,
                                         @Field("userPhone") String phone,
                                         @Field("userType") int userType);


}

