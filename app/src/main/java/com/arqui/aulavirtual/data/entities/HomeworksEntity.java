package com.arqui.aulavirtual.data.entities;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by kath on 2/06/18.
 */

public class HomeworksEntity implements Serializable {

    private int id;
    private String name;
    private ArrayList<ModuleEntity> modules;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<ModuleEntity> getModules() {
        return modules;
    }

    public void setModules(ArrayList<ModuleEntity> modules) {
        this.modules = modules;
    }

    /* public String getExpiration(){
        if (getJobOfferExpiration() == null ){
            return "";
        }
        Date tempDate = null;
        SimpleDateFormat parseDateFromServer= new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat parseDateForShowDetail =  new SimpleDateFormat("dd' de 'MMMM' del 'yyyy", new Locale("es","ES"));

        try {
            tempDate = parseDateFromServer.parse(getJobOfferExpiration());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return parseDateForShowDetail.format(tempDate);
    }*/
}
